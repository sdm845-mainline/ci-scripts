# Common CI scripts for the SDM845 repos

A repo with a few quick general purpose CI scripts, simply add a `.gitlab-ci.yml` to your project with one of the examples below, sit back, and relax (while shellcheck calls you out).

## Limitations

The defaults defined in the preamble will be overwritten if:

a) you specify them in your `.gitlab-ci.yml`
b) you specify them in your job definition

If stuff is behaving weirdly, make sure you didn't accidentally override the `before_script`.

## `shellcheck.sh`

Runs shellcheck on all `.sh` scripts, and some other specifics. Set `$SC_CUSTOM_FILES` in your CI environment to add custom files (that don't match `*.sh`).

### Example usage:

https://gitlab.com/sdm845-mainline/pmos-installer/-/blob/master/.gitlab-ci.yml

```yml
variables:
  SC_CUSTOM_FILES: |
    "
    pmenv
    pmenv.custom
    "

include:
  - 'https://gitlab.com/sdm845-mainline/ci-scripts/-/raw/master/templates/preamble.yml'
  - 'https://gitlab.com/sdm845-mainline/ci-scripts/-/raw/master/templates/shellcheck.yml'
```

## `buildkernel.sh`

Do an aarch64 kernel cross compile using the defconfig specified by the `KERNEL_DEFCONFIG` environment variable.

### Example usage:

https://gitlab.com/sdm845-mainline/sdm845-linux/-/blob/gitlab-ci/.gitlab-ci.yml
```yml
variables:
  - KERNEL_DEFCONFIG: "defconfig sdm845.config"

include:
  - 'https://gitlab.com/sdm845-mainline/ci-scripts/-/raw/master/templates/preamble.yml'
  - 'https://gitlab.com/sdm845-mainline/ci-scripts/-/raw/master/templates/kernel.yml'

build_kernel:
  artifacts:
    paths:
      - .output/arch/arm64/boot/Image.gz
      - .output/arch/arm64/boot/dts/qcom/sdm845-oneplus-enchilada.dtb
      - .output/arch/arm64/boot/dts/qcom/sdm845-oneplus-fajita.dtb
      - .output/arch/arm64/boot/dts/qcom/sdm845-xiaomi-beryllium.dtb
```