#!/bin/sh

# Build kernel and upload artifacts, using the pmtools repo
# Assumes we're IN the kernel source directory

TOOLSDIR="$(mktemp -d)"
export TOOLSDIR;


cd "$CI_PROJECT_DIR" || exit
git clone -q --depth 1 https://gitlab.com/sdm845-mainline/pmtools "$TOOLSDIR"

# shellcheck source=/dev/null
. "$TOOLSDIR/pmenv" || exit 1

# Use eval because the defconfig may actually be a defconfig followed by some fragments
# we need to evaluate it FIRST otherwise it will be treated as a single argument and confuse make
# shellcheck disable=SC2086
eval mm $KERNEL_DEFCONFIG
mm
