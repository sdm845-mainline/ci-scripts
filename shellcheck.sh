#!/bin/sh

# Shamelessly "inspired" by https://gitlab.com/postmarketOS/pmaports/-/blob/master/.gitlab-ci/shellcheck.sh

export SC_FAIL=0

cd "$CI_PROJECT_DIR" || exit

sh_files="
	$(find . -name '*.sh')
"

do_shellcheck() {
	for file in $1; do
		if [ ! -e "$file" ]; then
			continue
		fi
		echo "Test with shellcheck: $file"
		shellcheck -e SC1008 -x "$file" || export SC_FAIL=1
	done
}

do_shellcheck "$sh_files"

if [ -n "$SC_CUSTOM_FILES" ]; then
	do_shellcheck "$SC_CUSTOM_FILES"
fi

exit $SC_FAIL